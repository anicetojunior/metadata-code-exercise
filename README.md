
# Metadata Data Engineer take-home [test](./Question.md)

This project fetches historical weather data for the last 5 days from the OpenWeatherMap API, processes and analyzes the data, and stores the results in an SQLite database. The analysis includes calculating the highest temperatures by location and month, as well as the average temperature, minimum temperature, and maximum temperature per day.

## Requirements


- Python 3.9
- Pandas
- Pytest
- Pyarrow
- Python-dotenv
- Requests


### Setup
- Clone the repository and navigate to the project directory.
- Install the required Python libraries:

```bash
  pip install -r requirements.txt
```
- Obtain an API key from the OpenWeatherMap website and add it to the API_KEY in [.env](./script/config/.env) variable in the script.

- The locations that will be analyzed were informed in [locations.json](./script/config/locations.json)

## Running the Script

### To run the script, execute the following command:
````
python main.py
````
Alternatively, you can use Docker to run the script. Make sure you have Docker and Docker Compose installed on your machine. Then, execute the following command:

````
docker-compose up
````

## Project Structure

- [main.py](./script/main.py): Workflow in python that fetches, processes, and analyzes the weather data, and stores the results in parquet files.

- [Dockerfile](./script/Dockerfile): Docker configuration file for creating a containerized version of the project.

- [docker-compose.yml](./script/docker-compose.yml): Docker Compose configuration file for running the containerized project.

- [requirements.txt](./script/requirements.txt): List of required Python libraries.

### Output
The script will store the fetched weather data, highest temperatures by location and month, and daily temperature statistics in parquet files. The parquet contain the following information:

- weather_data.parquet: stores the raw weather data with location, date, hour, and temperature columns.

- highest_temps.parquet: stores the highest temperatures by location and month, with location, month, and temperature columns.

- daily_statistics.parquet: stores the daily temperature statistics, including date, average temperature, minimum temperature, minimum temperature location, maximum temperature, and maximum temperature location columns.

## Data pipeline
The [main.py](./script/main.py) is responsible for orchestrating the data pipeline, which consists of reading data from the OpenWeather api [weatherDataFecher.py](./script/weatherDataFetcher.py), starting a data fetching thread for each location and the data returned without modification is stored in parquet files already performing data deduplication.

The second step is to perform an analysis to identify the location, date and temperature of the [highest temperatures](./script/weatherAnalyzer.py) reported by location and month.

An analysis of the average temperature, minimum temperature, location of the minimum temperature and location of the maximum temperature per day is carried out in the [weatherAnalyzer.py](./script/weatherAnalyzer.py), a. This analysis is stored in the daily_statistics.parquet file.

All data analyzed for the second and final steps are shown on the console for viewing and checking.

Data pipeline unit tests have been implemented in [test_weatherDataFetcher.py](./test/test_weatherDataFetcher.py ) and are part of the CI/CD pipeline specified in [.gitlab-ci.yml](.gitlab-ci.yml).

## Author

- [@anicetojunior](https://gitlab.com/anicetojunior/metadata-code-exercise)

