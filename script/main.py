import logging
import pandas as pd
from datetime import datetime, timedelta
import json
import requests
import requests_cache
from weatherDataFetcher import WeatherDataFetcher
from weatherAnalyzer import WeatherAnalyzer
from parquetSaver import ParquetSaver

# Set up logging
logging.basicConfig(filename='./log/weather_data.log', level=logging.DEBUG)

with open('./config/locations.json', 'r') as f:
    LOCATIONS = json.load(f)

def main():
    """
    Main function that fetches weather data, analyzes it, and saves the results to Parquet files.
    """
    expire_after = timedelta(hours=1) # Adjust cache expiry time according to your needs
    session = requests_cache.CachedSession(cache_name='weather_cache', backend='sqlite', expire_after=expire_after)

    fetcher = WeatherDataFetcher(session)
    weather_data = fetcher.fetch_last_5_days_data(session, LOCATIONS)
    df = fetcher.process_weather_data(weather_data)

    analyzer = WeatherAnalyzer()
    highest_temps = analyzer.highest_temps_by_location_and_month(df)
    print("Highest Temperatures by Location and Month:")
    print(highest_temps)

    daily_statistics = analyzer.daily_stats(df)
    print("Daily Statistics:")
    print(daily_statistics)

    saver = ParquetSaver()
    saver.save_to_parquet(df, './output/weather_data.parquet', ['location', 'date', 'dt'])
    saver.save_to_parquet(highest_temps, './output/highest_temps.parquet', ['location', 'month', 'temp'])
    saver.save_to_parquet(daily_statistics, './output/daily_statistics.parquet', ['date', 'avg_temp', 'min_temp', 'max_temp', 'min_temp_location', 'max_temp_location'])

if __name__ == '__main__':
    """
    If this script is the main module, call the main() function.
    """
    main()
