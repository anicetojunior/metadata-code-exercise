import pandas as pd
from datetime import datetime, timedelta
import os

class ParquetSaver:
    """
    A class used to save pandas DataFrames to Parquet files with deduplication.
    """
    def __init__(self):
        """
        Initializes the ParquetSaver class.
        """
        pass

    def deduplicate_data(self, df, subset):
        """
        Remove duplicate rows from the DataFrame based on the specified subset of columns.

        :param df: A pandas DataFrame.
        :param subset: A list of column names or a single column name to consider for identifying duplicates.
        :return: A pandas DataFrame with duplicate rows removed.
        """

        df = df.drop_duplicates(subset=subset, keep='last')
        return df

    def save_to_parquet(self, df, file_path, subset):
        """
        Save the input DataFrame to a Parquet file, deduplicating data if the file already exists.

        :param df: A pandas DataFrame to be saved.
        :param file_path: A string representing the file path where the Parquet file should be saved.
        :param subset: A list of column names or a single column name to consider for identifying duplicates.
        :return: A pandas DataFrame containing the deduplicated data.
        """
        # If the Parquet file exists, read it and concatenate with the new data
        if os.path.exists(file_path):
            existing_df = pd.read_parquet(file_path)
            df = pd.concat([existing_df, df], ignore_index=True)

        # Deduplicate data before saving
        df = self.deduplicate_data(df, subset)

        # Save the deduplicated data to the Parquet file
        df.to_parquet(file_path, index=False, compression='snappy')

        return df
