import pandas as pd

def read_parquet_file(file_path):
    df = pd.read_parquet(file_path)
    return df

def show_data(file):
    df = read_parquet_file('../output/'+ file)
    print(df.head())  # show 5 lines of DataFrame

if __name__ == '__main__':
    file_path = 'weather_data.parquet'
    show_data(file_path)

    file_path = 'highest_temps.parquet'
    show_data(file_path)

    file_path = 'daily_statistics.parquet'
    show_data(file_path)
