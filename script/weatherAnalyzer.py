import pandas as pd

class WeatherAnalyzer:
    """
    A class used to analyze weather data.
    """
    def __init__(self):
        """
        Initializes the WeatherAnalyzer class.
        """
        pass

    def highest_temps_by_location_and_month(self, df):
        """
        Calculate the highest temperatures for each location and month from the input DataFrame.

        :param df: A pandas DataFrame containing weather data with 'location', 'date', and 'temp' columns.
        :return: A pandas DataFrame containing the highest temperatures for each location and month.
        """

        df['month'] = df['date'].apply(lambda x: x.month)
        highest_temps = df.groupby(['location', 'month'])['temp'].max().reset_index()
        return highest_temps

    def daily_stats(self, df):
        """
        Calculate daily statistics for the input weather data DataFrame, including average, minimum,
        and maximum temperatures, and the locations with the minimum and maximum temperatures.

        :param df: A pandas DataFrame containing weather data with 'location', 'date', and 'temp' columns.
        :return: A pandas DataFrame containing the calculated daily statistics.
        """
        daily_stats = df.groupby(['date', 'location'])['temp'].agg(['mean', 'min', 'max']).reset_index()
        daily_stats = daily_stats.rename(columns={'mean': 'avg_temp', 'min': 'min_temp', 'max': 'max_temp'})

        grouped = daily_stats.groupby('date')
        result = grouped.agg({
            'avg_temp': 'mean',
            'min_temp': 'min',
            'max_temp': 'max'
            }).reset_index()

        min_temp_locations = grouped.apply(lambda x: x.loc[x['min_temp'].idxmin(), 'location']).reset_index(name='min_temp_location')
        max_temp_locations = grouped.apply(lambda x: x.loc[x['max_temp'].idxmax(), 'location']).reset_index(name='max_temp_location')

        result = pd.concat([result, min_temp_locations['min_temp_location'], max_temp_locations['max_temp_location']], axis=1)
        return result
