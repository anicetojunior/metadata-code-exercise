import logging
import pandas as pd
import requests
import requests_cache
from datetime import datetime, timedelta
import threading
import os
from dotenv import load_dotenv
import pathlib

# Get the absolute path of the config folder
config_folder = pathlib.Path(__file__).parent.joinpath('config')

# Load the .env file from the config folder
load_dotenv(dotenv_path=config_folder.joinpath('.env'))

# Retrieve the API key from the environment variables
API_KEY = os.getenv('API_KEY')

# Set up logging
logging.basicConfig(filename='./log/weather_data.log', level=logging.DEBUG)

class WeatherDataFetcher:
    """
    A class to fetch weather data from OpenWeatherMap API for given locations and dates.
    """

    def __init__(self, session):
        """
        Initialize the WeatherDataFetcher with a given requests session.

        :param session: The requests session to use for fetching data.
        """
        self.session = session

    def fetch_weather_data(self, session, location, date):
        """
        Fetch weather data for a given location and date.

        :param session: The requests session to use for fetching data.
        :param location: A dictionary containing latitude, longitude, and name of the location.
        :param date: The date for which to fetch weather data.
        :return: A JSON object containing the fetched weather data.
        """
        url = f'https://api.openweathermap.org/data/2.5/onecall/timemachine'
        params = {
            'lat': location['lat'],
            'lon': location['lon'],
            'dt': int(date.timestamp()),
            'appid': API_KEY,
            'units': 'metric',
        }
        response = session.get(url, params=params)
        if response.status_code != 200:
            raise Exception(f"Error fetching weather data for {location['name']}: {response.status_code}")
        logging.info(f"Fetched data for location {location} with status code {response.status_code}")
        return response.json()

    def fetch_location_date_weather_data(self, session, location, date, weather_data):
        """
        Fetch weather data for a location and date and append it to the given list.

        :param session: The requests session to use for fetching data.
        :param location: A dictionary containing latitude, longitude, and name of the location.
        :param date: The date for which to fetch weather data.
        :param weather_data: A list to append the fetched weather data to.
        """
        try:
            data = self.fetch_weather_data(session, location, date)
            weather_data.append((location['name'], date, data))
        except Exception as e:
            logging.error(str(e))

    def fetch_last_5_days_data(self, session, locations):
        """
        Fetch weather data for the last 5 days for the given locations.

        :param session: The requests session to use for fetching data.
        :param locations: A list of dictionaries containing latitude, longitude, and name of the locations.
        :return: A list containing tuples of location name, date, and fetched weather data.
        """
        today = datetime.now()
        dates = [today - timedelta(days=i) for i in range(1, 6)]

        weather_data = []
        threads = []

        for location in locations:
            logging.info(f"Fetching data for location {location}")
            for date in dates:
                thread = threading.Thread(target=self.fetch_location_date_weather_data, args=(session, location, date, weather_data))
                thread.start()
                threads.append(thread)

        # Wait for all threads to complete
        for thread in threads:
            thread.join()

        return weather_data

    def process_weather_data(self, weather_data):
        """
        Process the fetched weather data and convert it into a pandas DataFrame.

        :param weather_data: A list containing tuples of location name, date, and fetched weather data.
        :return: A pandas DataFrame containing the processed weather data.
        """
        df_list = []
        for location, date, data in weather_data:
            daily_data = data['hourly']
            daily_df = pd.DataFrame(daily_data)
            daily_df['location'] = location
            daily_df['date'] = date.date()
            df_list.append(daily_df)
            logging.debug(f"Fetched data for location {location} at date {date.date()} and hour {date.hour}")

        df = pd.concat(df_list)
        return df
