import pandas as pd
import os
import tempfile
import pytest
from script.parquetSaver import ParquetSaver

class TestParquetSaver:
    @pytest.fixture
    def parquet_saver(self):
        return ParquetSaver()

    @pytest.fixture
    def sample_df(self):
        data = {
            "id": [1, 2, 3, 4, 1, 2],
            "name": ["A", "B", "C", "D", "A", "B"],
        }
        return pd.DataFrame(data)

    def test_deduplicate_data(self, parquet_saver, sample_df):
        deduplicated_df = parquet_saver.deduplicate_data(sample_df, subset="id")

        assert deduplicated_df is not None, "Deduplicated DataFrame should not be None"
        assert len(deduplicated_df) == 4, "Deduplicated DataFrame should have 4 rows"

    def test_save_to_parquet(self, parquet_saver, sample_df):
        with tempfile.TemporaryDirectory() as tmpdir:
            file_path = os.path.join(tmpdir, "test.parquet")

            deduplicated_df = parquet_saver.save_to_parquet(sample_df, file_path, subset="id")

            assert deduplicated_df is not None, "Deduplicated DataFrame should not be None"
            assert len(deduplicated_df) == 4, "Deduplicated DataFrame should have 4 rows"
            assert os.path.exists(file_path), "Parquet file should be created"
