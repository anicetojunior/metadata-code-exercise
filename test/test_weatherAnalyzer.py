import pandas as pd
import pytest
from datetime import datetime
from script.weatherAnalyzer import WeatherAnalyzer

@pytest.fixture
def sample_weather_data():
    """
    A fixture to provide sample weather data for testing the WeatherAnalyzer class methods.

    :return: A pandas DataFrame containing sample weather data.
    """
    data = [
        {"location": "A", "date": datetime(2023, 1, 1), "temp": 10},
        {"location": "A", "date": datetime(2023, 1, 1), "temp": 12},
        {"location": "B", "date": datetime(2023, 1, 1), "temp": 20},
        {"location": "A", "date": datetime(2023, 1, 2), "temp": 15},
        {"location": "B", "date": datetime(2023, 1, 2), "temp": 18},
        {"location": "B", "date": datetime(2023, 1, 3), "temp": 22},
    ]
    return pd.DataFrame(data)

def test_highest_temps_by_location_and_month(sample_weather_data):
    """
    Test the highest_temps_by_location_and_month method of the WeatherAnalyzer class.

    :param sample_weather_data: A pandas DataFrame containing sample weather data.
    """
    analyzer = WeatherAnalyzer()
    highest_temps = analyzer.highest_temps_by_location_and_month(sample_weather_data)

    assert len(highest_temps) == 2, "There should be two rows in the result"
    assert highest_temps.iloc[0]["temp"] == 15, "Highest temperature for location A should be 15"
    assert highest_temps.iloc[1]["temp"] == 22, "Highest temperature for location B should be 22"

def test_daily_stats(sample_weather_data):
    """
    Test the daily_stats method of the WeatherAnalyzer class.

    :param sample_weather_data: A pandas DataFrame containing sample weather data.
    """
    analyzer = WeatherAnalyzer()
    daily_stats = analyzer.daily_stats(sample_weather_data)

    assert len(daily_stats) == 3, "There should be three rows in the result"
    assert daily_stats.iloc[0]["min_temp_location"] == "A", "The location with the minimum temperature on 2023-01-01 should be A"
    assert daily_stats.iloc[0]["max_temp_location"] == "B", "The location with the maximum temperature on 2023-01-01 should be B"
    assert daily_stats.iloc[1]["min_temp_location"] == "A", "The location with the minimum temperature on 2023-01-02 should be B"
    assert daily_stats.iloc[1]["max_temp_location"] == "B", "The location with the maximum temperature on 2023-01-02 should be A"
    assert daily_stats.iloc[2]["min_temp_location"] == "B", "The location with the minimum temperature on 2023-01-03 should be B"
    assert daily_stats.iloc[2]["max_temp_location"] == "B", "The location with the maximum temperature on 2023-01-03 should be B"
