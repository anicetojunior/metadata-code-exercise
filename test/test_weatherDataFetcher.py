import pytest
import requests
import requests_cache
from datetime import datetime, timedelta
from script.weatherDataFetcher import WeatherDataFetcher

# Mock locations data for testing
TEST_LOCATIONS = [
    {
        "name": "Test Location",
        "lat": 37.7749,
        "lon": -122.4194
    }
]

def test_fetch_weather_data():
    """
    Test if the fetch_weather_data method returns the expected weather data for a given location and date.
    """
    expire_after = timedelta(hours=1)
    session = requests_cache.CachedSession(cache_name='weather_cache', backend='sqlite', expire_after=expire_after)

    fetcher = WeatherDataFetcher(session)

    test_location = TEST_LOCATIONS[0]
    test_date = datetime.now() - timedelta(days=1)
    weather_data = fetcher.fetch_weather_data(session, test_location, test_date)

    assert weather_data is not None, "Weather data should not be None"
    assert "hourly" in weather_data, "Weather data should contain 'hourly' key"

def test_fetch_last_5_days_data():
    """
    Test if the fetch_last_5_days_data method returns the expected weather data for the last 5 days for given locations.
    """
    expire_after = timedelta(hours=1)
    session = requests_cache.CachedSession(cache_name='weather_cache', backend='sqlite', expire_after=expire_after)

    fetcher = WeatherDataFetcher(session)

    weather_data = fetcher.fetch_last_5_days_data(session, TEST_LOCATIONS)

    assert len(weather_data) == 5, "Should fetch data for the last 5 days"

def test_process_weather_data():
    """
    Test if the process_weather_data method processes the fetched weather data correctly and returns a properly formatted dataframe.
    """
    expire_after = timedelta(hours=1)
    session = requests_cache.CachedSession(cache_name='weather_cache', backend='sqlite', expire_after=expire_after)

    fetcher = WeatherDataFetcher(session)

    weather_data = fetcher.fetch_last_5_days_data(session, TEST_LOCATIONS)
    df = fetcher.process_weather_data(weather_data)

    assert len(df) > 0, "Dataframe should not be empty"
    assert "location" in df.columns, "Dataframe should have a 'location' column"
    assert "date" in df.columns, "Dataframe should have a 'date' column"
